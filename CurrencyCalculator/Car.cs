﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace CurrencyCalculator
{
    public class Car
    {
        const byte MAX_GEAR = 6;

        int numberOfWheels;
        int enginePower;
        string carBrand;
        bool hasLeftSteeringWheel;
        double price;
        string fuelType;
        bool state = false;
        private Color color; // private is not required as it is default
        byte gear = 0;

        public string ceva2 = "ceva2";
        public static string ceva = "ceva";

        public Car(int wheelCount, int enPower, string brand, bool leftWheel, double p, string fuel)
        {
            numberOfWheels = wheelCount;
            enginePower = enPower;
            carBrand = brand;
            hasLeftSteeringWheel = leftWheel;
            price = p;
            fuelType = fuel;
        }

        public void GearUp()
        {
            if (gear < MAX_GEAR)
            {
                gear++;
            }
            else
            {
                Console.WriteLine("Sorry you can't increase the gear more than " + MAX_GEAR);
            }
        }

        public void GearDown()
        {
            if (gear > 0)
            {
                gear--;
            }
            else
            {
                Console.WriteLine("Sorry you can't decrease the gear more than 0");
            }
        }

        public byte GetGear()
        {
            return gear;
        }

        public void SetGear(byte newGear)
        {
            gear = newGear;
        }


        public void SetColor(Color color)
        {
            this.color = color;
        }

        public Car(string brand)
        {
            carBrand = brand;
        }

        public void Start()
        {
            state = true;
        }

        public void Stop()
        {
            state = false;
        }

        public void CheckCarState()
        {
            string s = "";
            if (state)
            {
                s = "started";
            }
            else
            {
                s = "stopped";
            }
            Console.WriteLine("The car " + carBrand + " is " + s);
            //s = (state) ? "started" : "stopped";
        }

        public void PrintCar()
        {
            Console.WriteLine("Number of wheels " + numberOfWheels);
            Console.WriteLine("engine power " + enginePower);
            Console.WriteLine("car brand " + carBrand);
        }

        public void Move()
        {

        }

        public void ChangeDirection()
        {

        }

        public static bool CompareCars(Car c1, Car c2)
        {
            if (c1.carBrand == c2.carBrand)
            {
                return true;
            }
            return false;
        }

        public static void PrintCar(Car c1)
        {
            Console.WriteLine(c1.carBrand);
        }

        public static void PrintMessage()
        {
            Console.WriteLine("this is my message");
        }
    }
}
