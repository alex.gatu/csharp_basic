﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace CurrencyCalculator
{
    class Program
    {
        static int x;
        static bool y;
        static string z;

        // Compute converted value based on conversion rate, ammount, currency
        public static void ComputeValue(float cr, float am, string crc)
        {
            Console.WriteLine("The " + am + " " + crc + " are worth " + cr * am + " RON");
        }

        public static void Meth1()
        {
            Console.WriteLine(x);
            Console.WriteLine(y);
            Console.WriteLine(z);
        }

        public static void Meth1(int x)
        {
            Console.WriteLine("int");
        }

        public static void Meth1(double x)
        {
            Console.WriteLine("double");
        }

        public static void Meth1(string y, int x)
        {
            
        }

        public static void Meth1(int x, string y)
        {

        }

        // Method to generate from args the converted value 
        public static void CurrencyCalculator(string[] args)
        {
            string currency;
            float ammount;
            float conversionRate;

            if (args.Length == 3)
            {
                currency = args[0];
                conversionRate = float.Parse(args[1]);
                ammount = float.Parse(args[2]);
            }
            else
            {
                Console.WriteLine("The number of arguments are wrong! Expected exactly 3!");
                Console.WriteLine("Provide the currency: ");
                currency = Console.ReadLine();
                Console.WriteLine("Provide the ammount: ");
                ammount = float.Parse(Console.ReadLine());
                Console.WriteLine("Provide the conversion rate: ");
                conversionRate = float.Parse(Console.ReadLine());
            }

            ComputeValue(conversionRate, ammount, currency);
            Console.ReadKey();

        }

        public static void Foo(int x, double y)
        {
            Console.WriteLine("int,double");
        }

        public static void Foo(double x, int y)
        {
            Console.WriteLine("double,int");
        }

        public static void DrawFullShape(int width, int height, string c)
        {
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Console.Write(c);
                }
                Console.WriteLine("");
            }
        }

        public static void DrawFullShape(int width, int height)
        {
            DrawFullShape(width, height, "*");
        }

        public static void DrawFullShape(int l)
        {
            DrawFullShape(l, l);
        }

        public static void DrawFullShape(double l)
        {

        }

        public static bool CircleAreIdentical(Circle c1, Circle c2)
        {
            return (c1.GetRadius() == c2.GetRadius());

            //if (c1.GetRadius() == c2.GetRadius())
            //{
            //    return true;
            //}
            //return false;

        }

        static void Main(string[] args)
        {
            //CurrencyCalculator(args);
            //int x = 2;
            //double y = 3;
            //Foo(2, (double)3);
            //Foo(x,y);
            //string a = "";
            //int x = int(a);
            //DrawFullShape(5, 7);
            //DrawFullShape((double)5);
            //DrawFullShape(5, 7, "^");

            //Car c = new Car(2, 100, "suzuki", false, 1500, "motorina");
            //Car c2 = new Car("bemveu");
            //c.SetColor(Color.Indigo);
            //c.SetGear(4);
            //c.GearDown();
            //Console.WriteLine("Current gear is: " + c.GetGear());
            //c.GearUp();
            //Console.WriteLine("Current gear is: " + c.GetGear());
            //c.GearUp();
            //Console.WriteLine("Current gear is: " + c.GetGear());
            //c.PrintCar();
            //Car c2 = new Car();
            //c2.InitCar(4, 300, "passat", false, 9000, "benzina");
            //c2.PrintCar();

            //c.Start();
            //c.CheckCarState();
            //c2.CheckCarState();

            //Circle circle1 = new Circle();
            //Circle circle2 = new Circle();
            //circle1.SetRadius(5);

            //Circle circle3 = new Circle(5);
            //Console.WriteLine("The area of the circle is " + circle1.GetArea());
            //circle2.SetRadius(5);
            //Console.WriteLine("The area of the circle is " + circle2.GetArea());
            ////if (circle1 == circle2)
            ////{
            ////    Console.WriteLine("identical");
            ////}
            //Console.WriteLine("Circle radius is " + circle1.GetRadius());
            //Console.WriteLine("Circle radius is " + circle2.GetRadius());
            //Console.WriteLine(CircleAreIdentical(circle1, circle2));

            //Square s = new Square(5);
            //Square s1 = s;
            ////Console.WriteLine(s.GetArea());
            ////s.SetSide(7);
            ////Console.WriteLine(s.GetArea());

            //MyStruct ms = new MyStruct();
            //ms.Assign(100);
            ////Console.WriteLine(ms.ComputeSum());
            //Parallelipiped p1 = new Parallelipiped(5,2,3);
            //Console.WriteLine(" Total area is " + p1.GetTotalArea());
            //Parallelipiped p2 = p1;
            //if (p1.Equals(p2))
            //{

            //}
            //Car.CompareCars();
            //Car.PrintCar(c);
            Car.PrintMessage();

            Console.ReadKey();
        }
        
    }
}
