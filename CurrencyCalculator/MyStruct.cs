﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyCalculator
{
    public struct MyStruct
    {
        int number;
        string s;

        public void Assign(int number)
        {
            this.number = number;
        }

        public int ComputeSum()
        {
            int sum = 0;
            for (int i = 0; i< number; i++)
            {
                sum += i;
            }
            // return (int) number * (number + 1) / 2;
            return sum;
        }
    }
}
