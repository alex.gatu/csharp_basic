﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyCalculator
{
    class Square
    {
        double side;
        
        // default constructor - no params
        public Square()
        {

        }

        // constructor with field initialisation
        public Square (double side)
        {
            this.side = side;
        }

        public void SetSide(double side)
        {
            this.side = side;
              
        }

        public double GetArea()
        {
            return side * side;
        }

    }
}
