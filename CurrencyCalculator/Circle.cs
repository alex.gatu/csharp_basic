﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyCalculator
{
    public class Circle
    {
        double radius;

        // Constructor which has no argument
        public Circle()
        {

        }

        // Constructor which has argument
        public Circle(double radius)
        {
            this.radius = radius;
        }
        
        public void SetRadius(double r)
        {
            radius = r;
        }

        public double GetRadius()
        {
            return radius;
        }
        
        public double GetArea()
        {
            return 2 * Math.PI * radius;
        }
        
    }
}
