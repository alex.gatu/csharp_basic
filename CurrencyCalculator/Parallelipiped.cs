﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyCalculator
{
    public struct Parallelipiped
    {
        int width;
        int height;
        int length;
        

        public Parallelipiped(int width, int h, int length)
        {
            this.width = width;
            height = h;
            this.length = length;
        }

        public int GetVertices()
        {
            return 8;
        }

        public int GetTotalArea()
        {
            return 2 * length * width + 4 * height * length;
        }

        public int GetVolume()
        {
            return height * width * length;
        }
    }
}
