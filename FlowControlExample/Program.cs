﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlowControlExample
{
    class Program
    {
        // Check if a random number is odd or even
        static void IfExample()
        {
            Console.WriteLine("Starting IfExample exercise ...");
            // Get a new random 
            Random r = new Random();
            // Get a random number between 1 and 100
            int randomNumber = r.Next(1, 100);
            // Check if the generated number is even by using % (mod) operator
            if (randomNumber % 2 == 0)
            {
                Console.WriteLine("The number " + randomNumber + " is even!");
            }
            // If the number is odd - negation of the if statement
            else
            {
                Console.WriteLine("The number " + randomNumber + " is odd!");
            }
            // You can also define a string variable which can be filled with "odd" or "even" depending on the if branch and put the WriteLine at the end
        }

        // Determine based on a random number what is number of the week day
        static void SwitchExample()
        {
            Console.WriteLine("Starting SwitchExample exercise ...");
            // Get a new random 
            Random r = new Random();
            // Get a random number between 1 and 8
            int randomNumber = r.Next(1, 8);
            // Depending on the random generated number the respective WriteLine will be printed
            switch(randomNumber)
            {
                case 1:
                    {
                        Console.WriteLine("Monday");
                        // On every case at the end the break; statement must be placed
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("Tuesday");
                        break;
                    }
                case 3:
                    {
                        Console.WriteLine("Wednesday");
                        break;
                    }
                case 4:
                    {
                        Console.WriteLine("Thursday");
                        break;
                    }
                case 5:
                    {
                        Console.WriteLine("Friday");
                        break;
                    }
                case 6:
                    {
                        Console.WriteLine("Saturday");
                        break;
                    }
                case 7:
                    {
                        Console.WriteLine("Sunday");
                        break;
                    }
                // if none of the above was matched - all the values will match the default block
                default:
                    {
                        Console.WriteLine("The value is not valid - should be between 0->6 and it is " + randomNumber);
                        break;
                    }
            }
        }

        // Print all items from a list
        static void ForExample()
        {
            Console.WriteLine("Starting ForExample exercise ...");
            string[] list = { "abc", "aaa", "motocoasa", "altceva", "oaie", "saizecisinoua" };
            for (int i = 0; i< list.Length; i++)
            {
                Console.WriteLine("The variable item has now the value " + list[i] + " and the idex of the loop is " + i);
            }
        }
        
        // Loop until a even number is found
        static void WhileExample()
        {
            Console.WriteLine("Starting WhileExample exercise ...");
            // Setting a flag to true as iniatial value
            bool flag = true;
            Random r = new Random();
            // As long as the flag has the value true the loop will be executed
            while (flag)
            {                
                // Get a random number between 1 and 1000
                int randomNumber = r.Next(1, 1000);
                // if the number is even the loop will end otherwise search for another number
                if (randomNumber % 2 == 0)
                {
                    // setting the flag to false will cause the while to break
                    flag = false;
                    Console.WriteLine("Found a even number and this is " + randomNumber + " The loop will stop!");
                }
                else
                {
                    Console.WriteLine("Searching for another random number that might be even - current one is " + randomNumber);
                }                
            }
        }
        
        // Loop until a even number is found 
        static void DoWhileExample()
        {
            Console.WriteLine("Starting DoWhileExample exercise ...");

            bool flag;
            Random r = new Random();
            // in do-while the code block will execute at least once
            do
            {
                // Get a random number between 1 and 1000
                int randomNumber = r.Next(1, 1000);
                if (randomNumber % 2 == 0)
                {
                    // setting the flag to false will cause the while to break
                    flag = false;
                    Console.WriteLine("Found a even number and this is " + randomNumber + " The loop will stop!");
                }
                else
                {
                    // setting the flag to true will trigger another loop
                    flag = true;
                    Console.WriteLine("Searching for another random number that might be even - current one is " + randomNumber);
                }

            }
            // as long as the flag is true
            while (flag);

        }
        
        // Print all items from a list
        static void ForeachExample()
        {
            Console.WriteLine("Starting ForeachExample exercise ...");
            string[] list = {"abc", "aaa", "motocoasa", "altceva", "oaie", "saizecisinoua" };
            // at each loop, the variable declared in foreach (string item) will take one of the values from the list in order
            foreach (string item in list)
            {
                Console.WriteLine("The variable item has now the value " + item);
            }
        }

        // Print the arguments of the method
        static void ArgumentsExample(int x, string y)
        {
            Console.WriteLine("Starting ArgumentsExample exercise ...");
            Console.WriteLine(" The first argument is " + x);
            Console.WriteLine(" The second argument is " + y);
        }

        // This is the entry point of the program - everything starts here when executing the program
        static void Main(string[] args)
        {
            // Call the method called IfExample
            IfExample();

            // Call the method called SwitchExample
            SwitchExample();

            // Call the method called ForExample
            ForExample();

            // Call the method called WhileExample
            WhileExample();

            // Call the method called DoWhileExample
            DoWhileExample();

            // Call the method called ForeachExample
            ForeachExample();

            //  Call the method called ArgumentsExample
            ArgumentsExample(1, "abc");

            // Wait for a key from the keyboard
            Console.ReadKey();
        }
    }
}
